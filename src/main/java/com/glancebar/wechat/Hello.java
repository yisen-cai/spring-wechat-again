package com.glancebar.wechat;

/**
 * Hello class
 *
 * @author Ethan Gary
 */
public class Hello {

    /**
     * show message
     */
    public static void showMsg() {
        System.out.println("Hello World!");
    }
}
